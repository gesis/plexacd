# Unfortunately, Amazon has banned pretty much everything from accessing their services so these scripts aren't very useful anymore. They can be modified easily to support using Google Drive [just change the remotename to your rclone configured drive account]. I am only leaving them up for posterity. New scripts with better support for Google Drive have been posted to https://reddit.com/r/PlexACD #

# PlexACD - A set of management scripts for maintaining a Plex + Amazon Cloud Drive based PVR system.

## NECESSARY UTILITIES

* Amazon Cloud Drive subscription
* Plex Media Server
* Working Linux Installation
* GNU coreutils (usually installed by default on Linux)
* Some version of the Bourne Shell [/bin/sh]
* rclone [version 1.34 or greater]
* PlexACD
* encfs and unionfs FUSE filesystems

## STEP 1 - Install unionfs.

Install the FUSE unionfs filesystem package for your distribution.

## STEP 2 - Install and configure encfs.

Install the FUSE encfs filesystem package for your distribution and create a new encfs filesystem under `~/.local-encrypted` and `~/.local-decrypt` by issuing the following command:

`mkdir ~/.local-encrypt ~/.local-decrypt`

`encfs ~/.local-encrypt ~/.local-decrypt`

*Use the default settings. ACD has issues with most of the advanced choices*

## STEP 3 - Move encryption keys.

Move the `.encfs6.xml` file from `~/.local-encrypt` to your home directory with the following command:

`mv ~/.local-encrypt/.encfs6.xml ~`

## STEP 4 - Install rclone.

[Install rclone](http://rclone.org/install/) and configure oauth login for your ACD account.

## STEP 5 - Install plex.

[Instructions here](https://support.plex.tv/hc/en-us/articles/200288586-Installation).

Skip down to the "Server Installation" section, and follow the directions for your Linux distro of choice. Sadly, the plex team did not add anchors to the page for easy reference. Use your mouse or something.

*When configuring Plex, you should store your libraries under `~/media`. This is the only directory which gets synced to ACD.*

## STEP 6 - Install PlexACD management scripts.

Create Amazon Cloud Drive mountpoints:

`mkdir ~/.acd-encrypt ~/.acd-decrypt`

Download and install [PlexACD](https://bitbucket.org/gesis/plexacd) management scripts via the following commands:

`git clone https://gesis@bitbucket.org/gesis/plexacd.git`

`mkdir -p ~/.config/PlexACD ~/bin`

`cd plexacd`

`cp plexacd.conf ~/.config/PlexACD`

`cp checkmount mount.remote nukedupes plexacd.sh updatecloud ~/bin`

## STEP 7 - Edit your configuration.

Edit the file `~/.config/PlexACD/plexacd.conf` to reflect your local setup. Most importantly, you will likely have to change the binary locations for `rclone`, `encfs` and `unionfs`. You can also skip making the directories above and use whatever other naming scheme you'd like... just change the variables here to reflect it.

*This file is sourced by the management scripts and contains variables describing files locations. Variable names should be descriptive enough to figure out. If not, please let me know.*

## STEP 8 - Mount filesystems.

run `~/bin/mount.remote`

This will mount the root of your ACD under `~/.acd-encrypt` as an encrypted filesystem and under `~/.acd-decrypt` as plaintext. It will also configure encfs encryption locally under `~/.local-encrypt` and `~/.local-decrypt` as well.

The local and remote decrypted filesystems will then be merged as `~/media`.

**THIS IS WHERE YOU KEEP ALL YOUR GOODIES.**

## STEP 9 - Configure cron.

Edit your crontab to run all the things... here's what mine looks like as an example...


    @reboot /home/gesis/bin/mount.remote &>> ~/logs/mount.log
    @hourly /home/gesis/bin/updatecloud &>> ~/logs/cloudupdates.log

    */5 * * * * /home/gesis/bin/checkmount &>> ~/logs/checkmount.log
    17 */1 * * * /home/gesis/bin/nukedupes &>> ~/logs/nukedupes.log

----

If you are using couchpotato, sonarr, sickbeard, etc... you should configure them to move renamed media to subdirectories under `~/media`. I do this with categories for each media type and use the built-in file renaming of each program. These scripts are not designed to manage your media, only to make sure it stays encrypted and synced.